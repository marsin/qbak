#!/bin/bash

# Quick Backup (qbak) - version 1.0
# a backup script for simple archiving local files quick
#
# Requires: tar, gzip, date, mkdir, chmod
#
# Copyright (c) 2014 Martin Singer <martin.singer@web.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

if [ $# -lt 1 ]
then
	>&2 echo "Try '${0} --help' for more information"
	exit 1
fi


# Varibles
src=""      # the source file or directory (relative or absolute)
prefix=""   # the prefix defined by -b, -o or -s (BAK_, ORG_ or SAVE_)
suffix="A"  # the suffix {A..Z} will be changed if file exists
ext=""      # the extension .gz is used for archived files, .tar.gz for dirs
dir="."     # the target directory

target=""	# the assembled target path

# Flags
archive="false"
quiet="false"
writeable="false"

# Check parameter
while [ "${1}" != "" ]
do
	case ${1} in
		"-b"|"--bak")
			prefix="BAK_"
			;;

		"-s"|"--save")
			prefix="SAVE_"
			;;

		"-o"|"--org")
			prefix="ORG_"
			;;

		-w|--writeable)
			writeable="true"
			;;

		--dir=*)
			dir=${1#*=}
			;;

		-d|--dir)
			dir=${2}
			shift
			;;

		-a|--archive)
			archive="true"
			;;

		-q|--quiet)
			quiet="true"
			;;

		"-h"|"--help")
			echo "Usage: $0 [OPTION]... [FILE]"
			echo "$0 archives files or directories with timestamp."
			echo "Files became a .gz, directories a .tar.gz"
			echo ""
			echo "  -o, --org         adds prefix 'ORG_'"
			echo "  -b, --bak         adds prefix 'BAK_'"
			echo "  -s, --save        adds prefix 'SAVE_'"
			echo ""
			echo "      --dir=PATH"
			echo "  -d, --dir PATH    specifies path of target directory"
			echo "                    default is current directory"
			echo "  -a, --archive     adds directory 'ARCHIVE' to target path"
			echo "                    creates directory 'ARCHIVE' if it does not exist already"
			echo ""
			echo "  -w, --writeable   keeps created archive writeable"
			echo "                    a created archive became write protectet by default"
			echo "  -q, --quiet       prints no messages to stdout"
			echo ""
			echo "Examples:"
			echo "${0} -o config.rc"
			echo "  This will create an archive named config.rc_ORG-2014.10.06.18:03:46.gz"
			echo ""
			echo "${0} -b -a project"
			echo "  If project is a directory, this will create an archive named"
			echo "    'ARCHIVE/project_BAK_2014-10-06_18-05-23.tar.gz'."
			echo "  The local directory 'ARCHIVE' will be createt if it does not exist."
			echo "  The same command executed a second time in the same second will create"
			echo "    'ARCHIVE/project_BAK_2014-10-06_18-05-23_A.tar.gz' (note the '_A')."
			echo ""
			echo "${0} -s -a -d /mnt/ project"
			echo "  This will create '/mnt/ARCHIVE/project_SAVE_2014-10-06_18-08-31.tar.gz'"
			echo ""
			echo "Requires: tar, gzip, date, chmod, mkdir"
			echo ""
			echo "Eit status:"
			echo "  0 if OK"
			echo "  1 if an error occured"
			echo ""
			echo "Report ${0} bugs to martin.singer@web.de"
			echo "Copyright (c) 2014 Martin Singer"
			echo "${0} is distributed under the terms of the GNU GPLv3+"
			echo "  It comes WITHOUT ANY WARRANTY!"
			exit 0
			;;

		*)
			if [ -f "${1}" ] || [ -d "${1}" ]
			then
				if [ "$src" == "" ]
				then
					src="${1}"
				else
					>&2 echo "${0}: Second source file or directory '${1}'."
					>&2 echo "First source file or directory is '${src}'."
					exit 1
				fi
			else
				>&2 echo "${0}: Unknown parameter: ${1}"
				>&2 echo "Try '${0} --help' for more information."
				exit 1
			fi
			;;
	esac
	shift
done

if [ "${src}" == "" ]
then
	>&2 echo "${0}: No source file or directory."
	exit 1
fi

# Expand relative paths to absolute pahts
src=$(readlink -m ${src})
dir=$(readlink -m ${dir})

# Check pahts
if [ -f "${src}" ]
then
	if [ ! -r "${src}" ]
	then
		>&2 echo "${0}: Source file '${src}' is not readable."
		exit 1
	fi
	ext="gz"
elif [ -d "${src}" ]
then
	if [ ! -r "${src}" ]
	then
		>&2 echo "${0}: Soruce directory '${src}' is not readable."
		#TODO: Check all files in src dir for readability, too!
		exit 1
	fi
	ext="tar.gz"
else
	>&2 echo "${0}: Source file or directory '${src}' doesn't exist."
	exit 1
fi

if [ "$archive" != "false" ]
then
	if [ -d "${dir}/ARCHIVE" ]
	then
		dir="${dir}/ARCHIVE"
	else
		if [ -f "${dir}/ARCHIVE" ]
		then
			>&2 echo "${0}: Path '${dir}/ARCHIVE' exists but is no directory."
			exit 1
		elif [ -w "${dir}" ]
		then
			test "${quiet}" == "false" && echo "mkdir ${dir}/ARCHIVE"
			mkdir "${dir}/ARCHIVE"
			if [ "$?" != "0" ]
			then
				>&2 echo "${0}: mkdir returned error."
				exit 1
			fi
			dir="${dir}/ARCHIVE"
		fi
	fi
fi

if [ -d ${dir} ]
then
	if [ ! -w "${dir}" ]
	then
		>&2 echo "${0}: Target directory '${dir}' is not writeable."
		exit 1
	fi
else
	>&2 echo "${0}: Target '${dir}' is no directory and/or does not exist."
	exit 1
fi

src_file=${src##*/} # Cut src path to src file name

# TEST
#echo "src     : $src"
#echo "src_file: $src_file"
#echo "dir     : $dir"

# Create output path and file name
# If a file name already exists check for a other suffix
	target=${dir}/${src_file}_${prefix}$(date +"%Y-%m-%d_%H-%M-%S").${ext}
for suffix in {A..Z}
do
	test -f $target || break
	target=${dir}/${src_file}_${prefix}$(date +"%Y-%m-%d_%H-%M-%S")_${suffix}.${ext}
done

#TEST
#echo "target: '$target'"

# The following if-check will be true if the for-loop leaves without
# the break-command. This happens if all backup-files including
# the suffix 'Z' already exist.
if [ -f "${target}" ]
then
	>&2 echo "${0}: '$target' already exist. No backup will be created."
	exit 1
fi

# Create the backup archive
# a file becomes a .gz, a directory becomes a .tar.gz
if [ -f "${src}" ]
then
	test "${quiet}" == "false" && echo "gzip --stdout ${src} > ${target}"
	gzip --stdout ${src} > $target
elif [ -d "${src}" ]
then
	test "${quiet}" == "false" && echo "tar cfz ${target} ${src}"
	tar cfz ${target} ${src}
fi

# Writeprotect archive
if [ -f "${target}" ]
then
	if [ "${writeable}" == "false" ]
	then
		test "${quiet}" == "false" && echo "chmod -w ${target}"
		chmod -w "${target}"
	fi
else
	>&2 echo "${0}: An error occurred! '${target}' does not exist."
	exit 1
fi

exit 0
